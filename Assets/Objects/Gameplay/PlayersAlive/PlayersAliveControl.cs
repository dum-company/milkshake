using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayersAliveControl : MonoBehaviour
{
    [SerializeField] GameObject victoryCanvas;
    [SerializeField] TextMeshProUGUI victoryText;
    [SerializeField] int roundLimit = 5;
    private List<GameObject> players;
    private int maxPlayers = 2;


    void Start()
    {
        maxPlayers = PlayerPrefs.GetInt("MaxPlayers", 2);
        players = new List<GameObject>();
    }

    void Update()
    {
        if (players.Count <= 1 && Time.timeScale != 0f)
        {
            Time.timeScale = 0f;
            victoryCanvas.SetActive(true);
            if (players.Count < 1)
            {
                victoryText.text = "It's a tie!!";
                return;
            }
            victoryText.text = players[0].name + " wins this round!!";
            addScore(players[0]);
            determineWinner(players[0]);
        }
    }

    void addScore(GameObject player){
        int score = PlayerPrefs.GetInt(player.name, 0);
        score++;
        PlayerPrefs.SetInt(player.name, score);
    }

    void determineWinner(GameObject player){
        int score = PlayerPrefs.GetInt(player.name, 0);
        if (score == roundLimit)
        {
            victoryText.text += "\n" + player.name + " wins the game!!";
            for (int i = 1 ; i <= maxPlayers ; i++)
                PlayerPrefs.DeleteKey("Player" + i);
        }
    }

    public void ConnectPlayer(params object[] args)
    {
        if ((bool) args[1])
            players.Add((GameObject) args[0]);
        else
            if (players.Contains((GameObject) args[0]))
                players.Remove((GameObject) args[0]);
    }

    public void RestartScene(){
        int scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
