using UnityEngine;
using System.Collections.Generic;
using Cinemachine;
using Target = Cinemachine.CinemachineTargetGroup.Target;

public class TargetGroupReference : MonoBehaviour
{
    [SerializeField] CinemachineTargetGroup targetGroup;

    public void SetTargetGroup(params object[] args)
    {
        List<Target> targets = new List<Target>();
        foreach (Target t in targetGroup.m_Targets)
            targets.Add(t);

        Target playerTarget = new Target{ target = (Transform) args[0], weight = 1f, radius = 1f };
        targets.Add(playerTarget);
        targetGroup.m_Targets = targets.ToArray();
    }
}
