using System.Collections.Generic;
using UnityEngine;

public class SpawnScreen : MonoBehaviour
{
    [SerializeField] private GameObject playerConnectionPrefab;
    [SerializeField] private RectTransform[] positions;
    private List<PlayerConnectionDisplay> connectionDisplays = new List<PlayerConnectionDisplay>();
    private int connectedPlayers = 0;


    void Start()
    {
        int maxPlayers = PlayerPrefs.GetInt("MaxPlayers", 2);
        for (int i = 0 ; i < maxPlayers ; i++)
        {
            GameObject go = GameObject.Instantiate(playerConnectionPrefab, positions[i]);
            connectionDisplays.Add(go.GetComponent<PlayerConnectionDisplay>());
            connectionDisplays[i].SetPlayerName("Player " + (i + 1));
        }
    }

    public void Ready(params object[] args)
    {
        if (!(bool) args[1])
            return;
        connectionDisplays[connectedPlayers].SetReadyColor();
        connectedPlayers++;
    }
}
