using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSpawn : MonoBehaviour
{

    [SerializeField] private GameEvent connectionEvent;
    [SerializeField] private GameEvent updateTargetGroup;

    [Header("Player Spawn")]
    [SerializeField] Transform[] playerSpawnPoints;
    [SerializeField] PlayerInputManager playerInputManager;
    
    [Header("SpawnScreen")]
    [SerializeField] GameObject spawnScreenCanvas;


    [Header("Player Count")]
    public int playerCount = 0;
    int maxPlayers;

    List<PlayerInput> players = new List<PlayerInput>();

    void Start()
    {
        maxPlayers = PlayerPrefs.GetInt("MaxPlayers", 2);
        initializeReadyScreen();
    }

    void initializeReadyScreen()
    {
        // pause the game 
        Time.timeScale = 0f;
        spawnScreenCanvas.SetActive(true);
    }

    public void JoinPlayer(PlayerInput player)
    {
        playerCount++;
        setupPlayer(player);
        
        if(playerCount >= maxPlayers)
        {
            readyAllPlayersAndStart();
            playerInputManager.DisableJoining();
        }

        Debug.Log(player.name + " joined");   
    }

    private void setupPlayer(PlayerInput player)
    {
        setupPlayerProperties(player);
        updateTargetGroup.Raise(player.transform);
        connectionEvent.Raise(player.gameObject, true);
    }

    private void setupPlayerProperties(PlayerInput player)
    {
        player.name = "Player" + playerCount;
        player.transform.position = playerSpawnPoints[playerCount - 1].position;

        player.gameObject.layer = LayerMask.NameToLayer("Player" + playerCount);
        player.SwitchCurrentActionMap("Null");
    
        players.Add(player);
    }

    void readyAllPlayersAndStart()
    {
        spawnScreenCanvas.SetActive(false);
        Time.timeScale = 1f;
        foreach(PlayerInput player in players)
            player.SwitchCurrentActionMap("Player");
    }
}
