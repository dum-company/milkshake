using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerConnectionDisplay : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Color readyColor;
    [SerializeField] private TMP_Text text;

    public void SetPlayerName(string name)
    {
        text.text = name;
    }

    public void SetReadyColor()
    {
        image.color = readyColor;
    }
}
