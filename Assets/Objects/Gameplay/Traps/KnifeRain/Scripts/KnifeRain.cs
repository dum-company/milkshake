using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KnifeRain : MonoBehaviour
{
    [SerializeField] private GameObject knifePrefab;
    [SerializeField] private SoundPlayer sP;
    private Transform[] positions;

    void Start()
    {
        List<Transform> transforms = new List<Transform>();
        foreach (Transform t in this.GetComponentsInChildren<Transform>())
            if (t.tag == "KnifeSpawn")
                transforms.Add(t);
        positions = transforms.ToArray();
    }

    public void SpawnKnives(Vector3 position)
    {
        this.transform.position = position;

        foreach (Transform pos in positions)
            Instantiate(knifePrefab, pos.position, Quaternion.identity);
        
        sP.PlaySound("Rain");
    }
}
