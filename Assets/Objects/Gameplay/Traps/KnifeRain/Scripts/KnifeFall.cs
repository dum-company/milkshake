using UnityEngine;

public class KnifeFall : MonoBehaviour
{
    [SerializeField] private GameEvent damageEvent;
    [SerializeField] int damage;
    [SerializeField] Rigidbody rigidBody;
    [SerializeField] float minimalForce;
    [SerializeField] float maximumForce;

    void Start()
    {
        rigidBody.AddForce(-transform.up * Random.Range(minimalForce,maximumForce));
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "Knife")
        {
            if (other.gameObject.tag == "Player")
                damageEvent.Raise(damage, other.gameObject.name);
            Destroy(this.gameObject);
        }
    }
}
