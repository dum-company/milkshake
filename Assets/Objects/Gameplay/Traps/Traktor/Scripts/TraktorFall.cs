using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraktorFall : MonoBehaviour
{
    [SerializeField] private GameEvent damageEvent;
    [SerializeField, Range(0, 20)] private int damage = 10;
    [SerializeField] Transform AttackPoint;
    [SerializeField] float attackRange;
    [SerializeField] GameObject smoke;

    void OnCollisionEnter(Collision collision)
    {
        explodeAsFuck();
    }

    void explodeAsFuck(){
        Collider[] playersHit = Physics.OverlapSphere(transform.position, attackRange);
        foreach (Collider player in playersHit)
        {
            Instantiate(smoke, transform.position, Quaternion.identity);
            if (player.gameObject.tag == "Player")
                damageEvent.Raise(damage, player.gameObject.name);
        }
        Destroy(this.gameObject);
    }

    void OnDrawGizmosSelected(){
        if (this.AttackPoint == null) return;
        Gizmos.DrawWireSphere(this.AttackPoint.position, this.attackRange);
    }
}
