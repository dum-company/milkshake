using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class SwarmMove : MonoBehaviour
{
    [SerializeField] private GameEvent damageEvent;
    [SerializeField] float speed;
    [SerializeField] int damage;

    void Update()
    {
        transform.Translate(transform.right * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            damageEvent.Raise(damage, other.gameObject.name);
        if (other.gameObject.tag == "FleaEnd")
            Destroy(transform.parent.gameObject);
        
    }
}
