using System.Collections;
using UnityEngine;

public class SwarmSpawner : MonoBehaviour
{
    [SerializeField] GameObject swarmPrefab;

    void Start()
    {
        Instantiate(swarmPrefab, transform.position, Quaternion.identity,transform);
    }
}
