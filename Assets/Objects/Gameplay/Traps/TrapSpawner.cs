using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TrapSpawner : MonoBehaviour
{
    [SerializeField] private GameObject traktorPrefab;
    [SerializeField] private float traktorSpawnHeight = 50f;
    [SerializeField] private KnifeRain knifeRain;
    [SerializeField] private float knifeSpawnHeight = 60f;
    [SerializeField] private GameObject swarmPrefab;

    [Header("Warn")]
    [SerializeField] private SoundPlayer soundPlayer;
    [SerializeField] private float warningTime;
    [SerializeField] private float spawnTime;
    [SerializeField] private GameObject alarm;
    [SerializeField] private Image signImage;
    [SerializeField] private Sprite[] signs;

    public void SpawnTrap(params object[] args)
    {
        StartCoroutine(warn((int) args[0]));
    }

    private IEnumerator warn(int id)
    {
        yield return new WaitForSeconds(warningTime);
        alarm.gameObject.SetActive(true);
        playSound(id);
        signImage.sprite = signs[id];
        StartCoroutine(spawnObject(id ));
    }

    private void playSound(int id)
    {
        if (id == 0)
            soundPlayer.PlaySound("Traktor");
        else if (id == 1)
            soundPlayer.PlaySound("Psicosis");
        else if (id == 2)
            soundPlayer.PlaySound("WhoLetTheDogsOut");
    }

    private IEnumerator spawnObject(int id)
    {
        yield return new WaitForSeconds(spawnTime);
        if (id == 0)
            spawnTraktor();
        else if (id == 1)
            spawnKnifes();
        else
            spawnSwarm();
        alarm.gameObject.SetActive(false);
    }

    void spawnTraktor()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
        Transform t = objs[Random.Range(0, objs.Length)].transform;
        GameObject.Instantiate(traktorPrefab, new Vector3(t.position.x, traktorSpawnHeight, t.position.z), Quaternion.identity);

    }

    void spawnKnifes()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
        Transform t = objs[Random.Range(0, objs.Length)].transform;
        knifeRain.SpawnKnives(new Vector3(t.position.x, knifeSpawnHeight, t.position.z));
    }

    void spawnSwarm()
    {
        GameObject.Instantiate(swarmPrefab);
    }
}
