using UnityEngine;

public class PowerUp : MonoBehaviour
{
    const int POWER_UP_AMOUNT = 6;
    const int WEAPON_POWERUPS = 3;
    [SerializeField] private GameEvent powerUpEvent;
    [SerializeField] private GameEvent trapEvent;
    [SerializeField] private SoundPlayer sP;
    private PowerUpSpawner spawner;

    public void SetSpawner(PowerUpSpawner spawn)
    {
        spawner = spawn;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            int random = Random.Range(0, POWER_UP_AMOUNT);
            if (random < WEAPON_POWERUPS)
            {
                powerUpEvent.Raise(random + 1, col.name);
                sP.PlaySound("PickUp");
            } else
            {
                trapEvent.Raise(random - 3);
            }
            Destroy(this.gameObject, 0.2f);
        }
    }

    void OnDestroy()
    {
        spawner.RemovePowerUp();
    }
}
