using UnityEngine;

public class BarbecueDamage : MonoBehaviour
{
    [SerializeField] private GameEvent damageEvent;
    [SerializeField] ParticleSystem[] smoke;
    [SerializeField] GameObject animeLines;
    [SerializeField, Range(0, 20)] private int damage;
    private bool timeOut = false;


    public void TimeOut()
    {
        timeOut = true;
    }

    void Update(){
        if (timeOut){
            transform.localScale += new Vector3(3f * Time.deltaTime, 0, 3f * Time.deltaTime);
            animeLines.SetActive(true);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player"){
            //collision.gameObject.GetComponent<PlayerHealth>().Damage(10);
            damageEvent.Raise(damage, collision.gameObject.name);
            foreach (ParticleSystem s in smoke)
            {   
                s.transform.position = collision.gameObject.transform.position;
                s.Play();
            }
        }
    }
}
