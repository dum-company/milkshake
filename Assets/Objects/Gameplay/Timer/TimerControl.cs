using UnityEngine;
using TMPro;

public class TimerControl : MonoBehaviour
{
    [SerializeField] private GameEvent timeOutEvent;
    [SerializeField] private int timeLimit;
    [SerializeField] TMP_Text[] timerTexts;
    int timer = 0;
    float time = 0;
    private bool timeOut = false;

    void Update()
    {            
        if(!timeOut)
            countDown();
        else 
            foreach(TMP_Text tmpText in timerTexts)
                tmpText.text = "BURGER TIME!!!";
    }

    private void countDown()
    {
        time += Time.deltaTime;
        timer = timeLimit - (int) Mathf.Floor(time);

        foreach(TMP_Text tmpText in timerTexts)
                tmpText.text = showFormattedTime(timer);

        if (timer <= 0)
        {
            timeOut = true;
            timeOutEvent.Raise();
        }
    }

    string showFormattedTime(int timer)
    {
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        return string.Format("{0:0}:{1:00}", minutes, seconds);
    }
}
