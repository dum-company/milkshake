using UnityEngine;

public class ChargeWeapon : MonoBehaviour, IPlayerWeapon
{
    #region interface
    public void UseWeapon(bool performed, float activeTimer)
    {
        timer = activeTimer;
        usingWeapon = performed;
        if (!usingWeapon && charging)
            stopCharge();
    }

    public void NoAmmo()
    {
        powerUpEvent.Raise(0, transform.parent.name);
        SetMaxAmmo();
    }

    public void SetMaxAmmo()
    {
        ammo = maxAmmo;
    }

    public float GetTimer()
    {
        return timer;
    }
    #endregion

    [SerializeField] private GameEvent powerUpEvent;
    [SerializeField] private GameEvent chargeEvent;
    [SerializeField] private Animator anim;
    [SerializeField] private SoundPlayer sP;
    //[SerializeField] private PlayerMovementScript playerMovement; //TODO: sadasd
    [SerializeField] private float chargeDuration;
    [SerializeField] private float cooldown;
    [SerializeField, Range(1, 5)] private int maxAmmo = 1;
    private int ammo;
    private float chargeTimer;
    float timer;
    private bool usingWeapon = false;
    private bool charging = false;

    void Start()
    {
        SetMaxAmmo();
    }

    void Update()
    {
        if (charging)
        {
            chargeTimer -= Time.deltaTime;
            if (chargeTimer <= 0)
                stopCharge();
            return;
        }

        if (timer > 0)
            timer -= Time.deltaTime;
        else if (usingWeapon && timer <= 0)
        {
            timer = cooldown;
            ammo--;
            startCharge();
        }
    }

    private void startCharge()
    {
        anim.SetBool("pauseCharge", false);
        charging = true;
        chargeEvent.Raise(charging, transform.parent.name);
        chargeTimer = chargeDuration;
        sP.PlaySound("Charge");
        sP.PlaySound("Fart");
    }

    private void stopCharge()
    {
        anim.SetBool("pauseCharge", true);
        charging = false;
        chargeEvent.Raise(charging, transform.parent.name);
        chargeTimer = 0;
        sP.StopSound("Charge");
        if (ammo <= 0 && usingWeapon)
            NoAmmo();
    }
}