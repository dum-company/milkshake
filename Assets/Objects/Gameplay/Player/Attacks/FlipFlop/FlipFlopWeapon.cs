using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class FlipFlopWeapon : MonoBehaviour, IPlayerWeapon
{
    #region interface
    public void UseWeapon(bool performed, float activeTimer)
    {
        usingWeapon = performed;
        timer = activeTimer;
    }

    public void NoAmmo()
    {
        powerUpEvent.Raise(0, transform.parent.name);
        SetMaxAmmo();
    }

    public void SetMaxAmmo()
    {
        ammo = maxAmmo;
    }

    public float GetTimer()
    {
        return timer;
    }
    #endregion

    [SerializeField] private GameEvent powerUpEvent;
    [SerializeField] private GameObject bullet;
    [SerializeField, Range(1, 5)] private int maxAmmo = 1;
    [SerializeField, Range(0, 5)] private float cooldown = 1f;
    private int ammo;
    float timer = 0;
    private bool usingWeapon = false;

    void Start()
    {
        SetMaxAmmo();
    }

    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        if (usingWeapon && timer <= 0)
        {
            timer = cooldown;
            ammo--;
            shoot();
            if (ammo <= 0)
                NoAmmo();
        }
    }

    private void shoot()
    {
        GameObject spawnedBullet = GameObject.Instantiate(bullet, transform.position, Quaternion.identity);
        spawnedBullet.layer = transform.parent.gameObject.layer;
        List<PlayerInput> players = GameObject.FindObjectsOfType<PlayerInput>().ToList();
        players.Remove(transform.parent.GetComponent<PlayerInput>());
        int random = Random.Range(0, players.Count - 1);
        spawnedBullet.GetComponent<FlipFlopBullet>().SetTarget(players[random].transform);
    }
}
