using UnityEngine;

public interface IPlayerWeapon
{    public void UseWeapon(bool performed, float activeTimer);
    public void NoAmmo();
    public void SetMaxAmmo();
    public float GetTimer();
}
