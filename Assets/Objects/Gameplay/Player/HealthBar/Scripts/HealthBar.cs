using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [Header("Colors")]
    [SerializeField] private CowColorPalette[] cowColors;

    [Header("Bar")]
    [SerializeField] private RectTransform self;
    [SerializeField] private Image weaponBackground;
    [SerializeField] private Image background;
    [SerializeField] private Image bar;
    [SerializeField] private Image dots;
    [SerializeField] private Image circle;
    [SerializeField] private Image weapon;
    [SerializeField] private Sprite[] weapons;
    private int playerID = -1;

    public void SetPosition(RectTransform rectTransform)
    {
        self.anchorMin = rectTransform.anchorMin;
        self.anchorMax = rectTransform.anchorMax;
        self.anchoredPosition = rectTransform.anchoredPosition;
    }

    public void SetID(int id)
    {
        playerID = id;
        weaponBackground.color = cowColors[playerID].primaryColor;
        background.color = cowColors[playerID].primaryColor;
        bar.color = cowColors[playerID].primaryColor;
        circle.color = cowColors[playerID].primaryColor;
        dots.color = cowColors[playerID].secondaryColor;
        weapon.color = cowColors[playerID].secondaryColor;
    }

    public void UpdateHealthBar(params object[] args)
    {
        if ((int) args[0] != playerID)
            return;
        if ((HealthEventTypes) args[1] == HealthEventTypes.health)
            setHealth((float) args[2]);
        if ((HealthEventTypes) args[1] == HealthEventTypes.weapon)
            setWeapon((int) args[2]);
    }

    private void setHealth(float value)
    {
        bar.fillAmount = value;
        dots.fillAmount = value;
    }

    private void setWeapon(int i)
    {
        weapon.sprite = weapons[i];
    }
}

public enum HealthEventTypes
{
    health,
    weapon
};