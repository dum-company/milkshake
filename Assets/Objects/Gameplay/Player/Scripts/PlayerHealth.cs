using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private GameEvent connectionEvent;
    [SerializeField] private GameEvent healthBarEvent;
    [SerializeField] private SoundPlayer sP;

    [Header("Bar")]
    [SerializeField] private RectTransform[] barPositions;
    [SerializeField] private GameObject barPrefab;

    [Header("Stats")]
    [SerializeField] private int maxHealth;
    private int currentHealth;
    private int playerID;

    void Start()
    {
        currentHealth = maxHealth;
    
        SetPlayerID(this.GetComponent<PlayerInput>().playerIndex);
    }

    public void SetPlayerID(int id) 
    {
        playerID = id;
        spawnBar();
    }

    private void spawnBar()
    {
        RectTransform elementTransform = barPositions[playerID];
        GameObject bar = GameObject.Instantiate(barPrefab, barPositions[playerID].parent);
        HealthBar hb = bar.GetComponent<HealthBar>();
        hb.SetPosition(elementTransform);
        hb.SetID(playerID);
    }

    public void Damage(params object[] args)
    {
        if ((string) args[1] != gameObject.name)
            return;
        currentHealth -= (int) args[0];
        sP.PlaySound("Hit");
        sP.PlaySound("Hurt");
        healthBarEvent.Raise(playerID, HealthEventTypes.health, ((float)currentHealth)/((float)maxHealth));
        if (currentHealth <= 0)
        {
            connectionEvent.Raise(this.gameObject, false);
            Destroy(this.gameObject);
        }
    }
}