using UnityEngine;
using UnityEngine.InputSystem;

public class CowTexture : MonoBehaviour {
    [SerializeField] private SkinnedMeshRenderer cowMesh;
    [SerializeField] private SkinnedMeshRenderer jacketMesh;
    [SerializeField] private Material cowMaterial;
    [SerializeField] private Material jacketMaterial;
    [SerializeField] private Sprite[] cowTextures;
    [SerializeField] private Sprite[] jacketTextures;
    private int playerID = -1;

    void Start()
    {
        changeTexture(this.GetComponent<PlayerInput>().playerIndex);
    }

    private void changeTexture(int id)
    {
        if (playerID != -1)
            return;
        playerID = id;

        Material newCowMaterial = new Material(cowMaterial);
        Material newJacketMaterial = new Material(jacketMaterial);

        newCowMaterial.mainTexture = cowTextures[playerID].texture;
        newJacketMaterial.mainTexture = jacketTextures[playerID].texture;

        cowMesh.material = newCowMaterial;
        jacketMesh.material = newJacketMaterial;
    }
    
}
