using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private GameEvent healthBarEvent;
    [SerializeField] private Animator anim;
    private IPlayerWeapon[] weapons;
    private int selectedWeapon = 0;
    private bool isUsing = false;
    private int playerID;

    void Awake()
    {
        weapons = GetComponentsInChildren<IPlayerWeapon>();
    }

    void Start()
    {
        playerID = this.GetComponent<PlayerInput>().playerIndex;
    }

    public void OnUse(InputAction.CallbackContext context)
    {
        isUsing = context.performed;
        setUsingWeapon();
    }

    private void setUsingWeapon()
    {
        anim.SetBool("attacking", isUsing);
        float activeTimer = weapons[selectedWeapon].GetTimer();
        useWeaponCall(selectedWeapon, isUsing, activeTimer);
    }

    //? (int)selection, (string)object.name
    public void ChangeWeapon(params object[] args)
    {
        if ((string) args[1] != gameObject.name)
            return;

        float activeTimer = weapons[selectedWeapon].GetTimer();
        setWeaponsState((int)args[0], activeTimer);

        healthBarEvent.Raise(playerID, HealthEventTypes.weapon, selectedWeapon);
    }

    private void setWeaponsState(int newSelection, float timer)
    {
        useWeaponCall(selectedWeapon, false, 0f);
        selectedWeapon = newSelection;
        weapons[selectedWeapon].UseWeapon(isUsing, timer);
        useWeaponCall(selectedWeapon, isUsing, timer);
        anim.SetInteger("weaponSelected", newSelection);
    }

    private void useWeaponCall(int weapon, bool isUsing, float timer)
    {
        weapons[weapon].UseWeapon(isUsing, timer);
    }
}
