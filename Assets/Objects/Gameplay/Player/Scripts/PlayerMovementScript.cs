using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementScript : MonoBehaviour
{
    [SerializeField] private GameEvent damageEvent;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Animator anim;
    [SerializeField] private SoundPlayer sP;

    [Header("Movement")]
    [SerializeField, Range(0, 3)] private float drag;
    [SerializeField] private float moveForceNormal;
    [SerializeField] private float moveForceDulceOfMilk;
    [SerializeField] private float barbecueMultiplier;
    [SerializeField, Range(0, 20)] private float maxVelocity;
    
    private Vector3 lookPosition = Vector3.zero;
    private bool moving = false;
    private float currentMoveForce;
    private string currentStepSound = "Step";


    [Header("Jump")]
    [SerializeField] private float jumpVelocity;
    [SerializeField] private float barbecueImpulse;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField, Range(0, 1)] private float groundCheckLenght;
    bool grounded = false;

    [Header("Charge")]
    [SerializeField, Range(0, 20)] private int chargeDamage;
    [SerializeField] private float chargeVelocity;
    [HideInInspector] private bool charging = false;

    private Transform barbecueTransform;
    private bool timeOut = false;
    
    public void SetCharge(params object[] args)
    {
        if ((string) args[1] == this.name)
            charging = (bool) args[0];
    }

    void Start()
    {
        barbecueTransform = GameObject.Find("BarbecueSpawnPoint").transform;//TODO: buscar como quitar esto

        rb.drag = drag;
        currentMoveForce = moveForceNormal;
    }

    void Update()
    {
        checkGround();
    }

    private void checkGround()
    {
        bool prev = grounded;
        grounded = Physics.Raycast(groundCheck.position, - Vector3.up, groundCheckLenght + 0.1f, groundLayer);
        if (grounded && !prev)
            sP.PlaySound("Fall");
    }

    void FixedUpdate()
    {
        move();
    }

    private void move()
    {
        if (charging)
        {
            rb.velocity = transform.forward * chargeVelocity;
            return;
        }
        
        if (moving)
        {
            sP.PlaySound(currentStepSound);
            rb.AddForce(transform.forward * currentMoveForce);
        }

        if (timeOut)
            goToBarbecue();

        clampVelocity();
    }

    void goToBarbecue()
    {
        Vector3 attractionDir = barbecueTransform.position - transform.position;
        rb.AddForce(attractionDir.normalized * currentMoveForce * barbecueMultiplier);
    }

    private void clampVelocity()
    {
        Vector3 vel = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        if (vel.magnitude > maxVelocity)
        {
            vel = Vector3.ClampMagnitude(vel, maxVelocity);
            rb.velocity = new Vector3(vel.x, rb.velocity.y, vel.z);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        Vector2 value = context.ReadValue<Vector2>();
        stopMoveSounds(value);

        if (charging)
            return;

        setPlayerRotation(value);
        setMovingStates(value);
    }

    private void stopMoveSounds(Vector2 value)
    {
        if (value != Vector2.zero)
            return;
        sP.StopSound("SweetStep");
        sP.StopSound("Step");
    }

    private void setPlayerRotation(Vector2 value)
    {
        lookPosition.x = transform.position.x + value.x;
        lookPosition.y = transform.position.y;
        lookPosition.z = transform.position.z + value.y;
        transform.LookAt(lookPosition, Vector3.up);
    }

    private void setMovingStates(Vector2 value)
    {
        moving = value.x != 0 || value.y != 0;
        anim.SetBool("walking", moving);
        anim.SetBool("idle", !moving);
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (grounded)
            rb.velocity = new Vector3(rb.velocity.x, jumpVelocity, rb.velocity.z);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawRay(groundCheck.position, - groundCheck.up * groundCheckLenght);
    }

    void OnCollisionEnter(Collision collision)
    {
        checkDulceOfMilk(collision.gameObject.tag);
        checkChargeCollision(collision.gameObject);
        checkBarbecue(collision.gameObject.tag);
    }

    private void checkDulceOfMilk(string tag)
    {
        if (tag == "DulceOfMilk")
        {
            currentMoveForce = moveForceDulceOfMilk;
            currentStepSound = "SweetStep";
            sP.StopSound("Step");
        } else if (tag == "Ground")
        {
            currentMoveForce = moveForceNormal;
            currentStepSound = "Step";
            sP.StopSound("SweetStep");
        }
    }

    private void checkChargeCollision(GameObject other)
    {
        if (charging && other.tag == "Player")
        {
            damageEvent.Raise(chargeDamage, other.name);
            sP.PlaySound("ChargeImpact");
        }
    }

    private void checkBarbecue(string tag)
    {
        if (tag == "Barbecue")
            rb.velocity = new Vector3(rb.velocity.x, barbecueImpulse, rb.velocity.z);
    }

    public void TimeOut()
    {
        timeOut = true;
    }
}
