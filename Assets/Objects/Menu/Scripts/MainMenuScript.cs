using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private GameEvent audioManagerEvent;
    [Header("Audio")]
    [SerializeField] private AudioMixer mixer;
    
    [Header("Animator")]
    [SerializeField] private Animator creditsAnimator;
    [SerializeField] private Animator modeAnimator;

    public void Play(int players)
    {
        setupInitialValues(players);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void setupInitialValues(int players)
    {
        PlayerPrefs.SetInt("MaxPlayers", players);
        for (int i = 1 ; i <= players ; i++)
            PlayerPrefs.DeleteKey("Player" + i);
    }

    public void UpdateSlider(float value)
    {
        mixer.SetFloat("Volume", Mathf.Log10(value) * 20);
    }

    public void SetCreditsAnimator(bool value)
    {
        creditsAnimator.SetBool("IsIn", value);
    }

    public void SetModeAnimator(bool value)
    {
        modeAnimator.SetBool("IsIn", value);
    }

    public void ButtonHover()
    {
        audioManagerEvent.Raise("Hover");
    }

    public void ButtonPressed()
    {
        audioManagerEvent.Raise("Click");
    }
}
