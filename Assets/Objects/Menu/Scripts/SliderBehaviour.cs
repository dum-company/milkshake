using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBehaviour : MonoBehaviour
{
    [SerializeField] private Image fill;
    private Slider slider;

    void Start()
    {
        slider = this.GetComponent<Slider>();
    }

    public void SliderUpdate(float value)
    {
        fill.fillAmount = value/slider.maxValue;
    }
}
