using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Event", menuName = "Events/Game Event", order = 2)]
public class GameEvent : ScriptableObject
{
    private readonly List<GameEventListener> eventListener = new List<GameEventListener>();

    public void Raise(params object[] args)
    {
        for (int i = 0; i < eventListener.Count; i++)
            eventListener[i].OnEventRaised(args);
    }

    public void RegisterEvent(GameEventListener listener)
    {
        if (!eventListener.Contains(listener))
            eventListener.Add(listener);
    }

    public void UnregisterEvent(GameEventListener listener)
    {
        if (eventListener.Contains(listener))
            eventListener.Remove(listener);
    }
}
