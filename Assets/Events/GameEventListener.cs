using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    [SerializeField] private GameEvent gameEvent;
    [SerializeField] private UnityEvent<object[]> response = new UnityEvent<object[]>();


    void Start()
    {
        gameEvent.RegisterEvent(this);
    }

    void OnDestroy()
    {
        gameEvent.UnregisterEvent(this);
    }

    public void OnEventRaised(params object[] args)
    {
        response.Invoke(args);
        //  se puede hacer
        //  typeOf(args[i]) obj = (typeOf(args[i])) args[i]
        //  o
        //  if(args[i].GetType() == typOf(int))
        //      int obj = (int) args[i];
    }
}
